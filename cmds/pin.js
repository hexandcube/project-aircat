const Discord = module.require("discord.js");
const config = require("./../config/config.json")

module.exports.run = async (client, message, args) => {

    if (!message.member.hasPermission("MANAGE_MESSAGES"))
        return message.reply(config.defAnswer[0]);

    var pinMessage = args.join(" ");
    message.delete().catch(O_o => {});
    message.channel.send(pinMessage).then(msg => msg.pin());
};

module.exports.help = {
    name: "pin"
}