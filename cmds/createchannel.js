const Discord = module.require("discord.js");
const appConfig = require("./../config/config.json");
const config = require("./../config/modules_config/createchannelConfig.json");
module.exports.run = async (client, message, args) => {

  if (!message.member.hasPermission("MANAGE_CHANNELS"))
      return message.reply(appConfig.defAnswer[0]);

  let channelname = args.join(" ");
  message.channel.guild.createChannel(channelname).then(function (channel) {
      message.channel.send(config.sys_createchannelSuccess + " " + channel);
  }).catch(function (error) {
      message.channel.send(config.sys_createchannelErr);
  });

  return;

};

module.exports.help = {
    name: "createchannel"
}