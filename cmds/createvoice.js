const Discord = module.require("discord.js");
const appConfig = require("./../config/config.json");
const config = require("./../config/modules_config/createvoiceConfig.json");

module.exports.run = async (client, message, args) => {

    if (!message.member.hasPermission("MANAGE_CHANNELS"))
        return message.reply(config.defAnswer[0]);

    let vchannelname = args.join(" ");
    message.channel.guild.createChannel(vchannelname, "voice").then(function (channel) {
        message.channel.send(config.sys_createvoiceSuccess + " " + vchannelname);
    }).catch(function (error) {
        message.channel.send(config.sys_createchannelErr);
    });

    return;

};

module.exports.help = {
    name: "createvoice"
}