const Discord = module.require("discord.js");
const config = module.require("../config/config.json");
module.exports.run = async (client, message, args) => {

    //TODO Parameters
    let parameters = args.join(" ");

    let accessdenied = new Discord.RichEmbed()
        .setFooter(config.name, config.logo)
        .setTitle(config.accessDenied[0])
        .setDescription(`**${message.author.username}#${message.author.discriminator}** doesn't have required permissions!\nOnly users with **Administrator** permission can execute this command!`)
        .setThumbnail(config.iconAccess)
        .setColor("#ff4c4c")


    let bcmsg = new Discord.RichEmbed()
        .setTitle(parameters[0])
        .setDescription(`Kill switch has been activated by **${message.author.username}#${message.author.discriminator}**\n\n Shutting down...`)
        .setThumbnail(config.iconInfo)
        .setColor("#4c96ff")
        .setFooter(config.name, config.logo)

    if (!message.member.hasPermission("ADMINISTRATOR"))
        message.channel.send(accessdenied);
        else broadcast();

    function broadcast() {
        // message.channel.send(bcmsg)
    };


};

module.exports.help = {
    name: "broadcast"
}